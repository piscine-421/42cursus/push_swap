/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:46:53 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	exec(t_stacks *s, char *str)
{
	if (!ft_strncmp(str, "sa\n\0", 4))
		inst_sa(s);
	else if (!ft_strncmp(str, "sb\n\0", 4))
		inst_sb(s);
	else if (!ft_strncmp(str, "ss\n\0", 4))
		inst_ss(s);
	else if (!ft_strncmp(str, "pa\n\0", 4))
		inst_pa(s);
	else if (!ft_strncmp(str, "pb\n\0", 4))
		inst_pb(s);
	else if (!ft_strncmp(str, "ra\n\0", 4))
		inst_ra(s);
	else if (!ft_strncmp(str, "rb\n\0", 4))
		inst_rb(s);
	else if (!ft_strncmp(str, "rr\n\0", 4))
		inst_rr(s);
	else if (!ft_strncmp(str, "rra\n\0", 5))
		inst_rra(s);
	else if (!ft_strncmp(str, "rrb\n\0", 5))
		inst_rrb(s);
	else if (!ft_strncmp(str, "rrr\n\0", 5))
		inst_rrr(s);
	else
		return (0);
	return (1);
}

static void	listen(t_stacks *s)
{
	char	*str;

	str = get_next_line(1);
	while (!str || ft_strncmp(str, "\n", 2))
	{
		if (!str)
		{
			free(s->a);
			free(s->b);
			error();
		}
		else if (!exec(s, str))
		{
			free(str);
			free(s->a);
			free(s->b);
			error();
		}
		free(str);
		str = get_next_line(1);
	}
	free(str);
}

int	main(int argc, char **argv)
{
	t_stacks	s;

	if (argc == 1)
		return (EXIT_SUCCESS);
	chkparams(argv);
	init_s(&s, argc, argv, 0);
	chksort(&s);
	listen(&s);
	if (chksort(&s))
		ft_printf("OK\n");
	else
		ft_printf("KO\n");
	free(s.a);
	free(s.b);
}
