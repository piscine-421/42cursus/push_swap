/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:47:00 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	main(int argc, char **argv)
{
	t_stacks	s;

	if (argc == 1)
		return (EXIT_SUCCESS);
	chkparams(argv);
	init_s(&s, argc, argv, 1);
	if (s.sizea == 2 && s.a[0] > s.a[1])
		inst_sa(&s);
	else if (!chksort(&s))
		sort(&s);
	free(s.a);
	free(s.b);
}
