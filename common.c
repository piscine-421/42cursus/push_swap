/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:46:55 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	error(void)
{
	ft_putstr_fd("Error\n", 2);
	exit(EXIT_FAILURE);
}

void	chkparams(char **argv)
{
	int		i;
	int		i2;
	char	*str;

	i = 1;
	while (argv[i])
	{
		i2 = 0;
		while (argv[i][i2])
		{
			if (!ft_isdigit(argv[i][i2]) && !(i2 == 0 && argv[i][i2] == '-'))
				error();
			i2++;
		}
		str = ft_itoa(ft_atoi(argv[i]));
		if (!str)
			error();
		if (ft_strncmp(argv[i], str, ft_strlen(argv[i])))
		{
			free(str);
			error();
		}
		free(str);
		i++;
	}
}

int	chksort(t_stacks *s)
{
	int	i;
	int	i2;

	i = 0;
	while (s->sizea >= i)
	{
		i2 = i + 1;
		while (s->sizea > i2)
		{
			if (s->a[i2] == s->a[i])
			{
				free(s->a);
				free(s->b);
				error();
			}
			i2++;
		}
		i++;
	}
	i = 0;
	while (++i < s->sizea)
		if (s->a[i - 1] >= s->a[i])
			return (0);
	return (!(s->sizeb));
}

void	init_s(t_stacks *s, int argc, char **argv, int print)
{
	int	i;

	s->sizea = (size_t)argc - 1;
	s->sizeb = 0;
	s->a = ft_calloc(s->sizea, sizeof(int));
	if (!s->a)
		error();
	s->b = ft_calloc(s->sizea, sizeof(int));
	if (!s->b)
	{
		free(s->a);
		error();
	}
	s->instnum = 0;
	s->print = print;
	i = 1;
	while (argv[i])
	{
		s->a[i - 1] = ft_atoi(argv[i]);
		i++;
	}
}

void	inst_sa(t_stacks *s)
{
	int	swap;

	if (s->sizea >= 2)
	{
		swap = s->a[0];
		s->a[0] = s->a[1];
		s->a[1] = swap;
	}
	s->instnum++;
	if (s->print)
		ft_printf("sa\n");
}
