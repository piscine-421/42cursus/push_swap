/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:46:57 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	inst_rb(t_stacks *s)
{
	int	i;
	int	swap;

	i = 0;
	swap = s->b[0];
	while (s->sizeb > i++)
		s->b[i - 1] = s->b[i];
	s->b[i - 2] = swap;
	s->instnum++;
	if (s->print)
		ft_printf("rb\n");
}

void	inst_rr(t_stacks *s)
{
	int	i;
	int	swap;

	i = 0;
	swap = s->a[0];
	while (s->sizea > i++)
		s->a[i - 1] = s->a[i];
	s->a[i - 2] = swap;
	i = 0;
	swap = s->b[0];
	while (s->sizeb > i++)
		s->b[i - 1] = s->b[i];
	s->b[i - 2] = swap;
	s->instnum++;
	if (s->print)
		ft_printf("rr\n");
}

void	inst_rra(t_stacks *s)
{
	int	i;
	int	swap;

	i = s->sizea;
	swap = s->a[i - 1];
	while (i--)
		s->a[i] = s->a[i - 1];
	s->a[0] = swap;
	s->instnum++;
	if (s->print)
		ft_printf("rra\n");
}

void	inst_rrb(t_stacks *s)
{
	int	i;
	int	swap;

	i = s->sizeb;
	swap = s->b[i - 1];
	while (i--)
		s->b[i] = s->b[i - 1];
	s->b[0] = swap;
	s->instnum++;
	if (s->print)
		ft_printf("rrb\n");
}

void	inst_rrr(t_stacks *s)
{
	int	i;
	int	swap;

	i = s->sizea;
	swap = s->a[i - 1];
	while (i--)
		s->a[i] = s->a[i - 1];
	s->a[0] = swap;
	i = s->sizeb;
	swap = s->b[i - 1];
	while (i--)
		s->b[i] = s->b[i - 1];
	s->b[0] = swap;
	s->instnum++;
	if (s->print)
		ft_printf("rrr\n");
}
