NAME1		:= push_swap
NAME2		:= checker
DEBUG_FLAGS	:= -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter -g
FLAGS		:= -Wall -Werror -Wextra
GET_NEXT_LINE	:= ./libs/get_next_line
LIBFT		:= ./libs/Libft
PRINTF		:= ./libs/ft_printf
LIB		:= $(PRINTF)/libftprintf.a $(LIBFT)/libft.a
SRC1		:= a_utils.c b_utils.c common.c common2.c common3.c $(GET_NEXT_LINE)/get_next_line_bonus.c $(GET_NEXT_LINE)/get_next_line_utils_bonus.c push_swap.c sort.c
SRC2		:= common.c common2.c common3.c checker_bonus.c $(GET_NEXT_LINE)/get_next_line_bonus.c $(GET_NEXT_LINE)/get_next_line_utils_bonus.c

all: $(NAME1)

bonus: $(NAME1) $(NAME2)

clean:
	@make clean -s -C $(PRINTF)
	@make clean -s -C $(LIBFT)
	@rm -rf $(NAME1).dSYM $(NAME2).dSYM

debug:
	@make debug -s -C $(PRINTF)
	@make debug -s -C $(LIBFT)
	@cc $(DEBUG_FLAGS) -g $(LIB) $(SRC1) -o $(NAME)
	@cc $(DEBUG_FLAGS) -g $(LIB) $(SRC2) -o $(NAME2)

fclean: clean
	@make fclean -s -C $(PRINTF)
	@make fclean -s -C $(LIBFT)
	@rm -rf $(NAME1) $(NAME2) $(PRINTF)/libftprintf.a $(LIBFT)/libft.a

re: fclean all

$(NAME1): $(SRC1)
	@make bonus -s -C $(PRINTF)
	@make bonus -s -C $(LIBFT)
	@cc $(FLAGS) $(LIB) $(SRC1) -o $(NAME1)

$(NAME2): $(SRC2)
	@make bonus -s -C $(PRINTF)
	@make bonus -s -C $(LIBFT)
	@cc $(FLAGS) $(LIB) $(SRC2) -o $(NAME2)

.PHONY: all, bonus, clean, debug, fclean, re, $(NAME1), $(NAME2),
