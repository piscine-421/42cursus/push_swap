/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:47:03 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include "libs/ft_printf/ft_printf_bonus.h"
# include "libs/get_next_line/get_next_line_bonus.h"
# include "libs/Libft/libft.h"
# include <limits.h>
# include <stdlib.h>
# include <unistd.h>

typedef struct s_stacks
{
	int	*a;
	int	*b;
	int	max;
	int	min;
	int	print;
	int	instnum;
	int	sizea;
	int	sizeb;
}	t_stacks;

void	chkparams(char **argv);
int		chksort(t_stacks *s);
void	error(void);
int		getindexa(t_stacks *s, int n);
int		getindexb(t_stacks *s, int n);
void	init_s(t_stacks *s, int argc, char **argv, int print);
void	inserta(t_stacks *s);
void	insertb(t_stacks *s, const int i);
void	inst_sa(t_stacks *s);
void	inst_sb(t_stacks *s);
void	inst_ss(t_stacks *s);
void	inst_pa(t_stacks *s);
void	inst_pb(t_stacks *s);
void	inst_ra(t_stacks *s);
void	inst_rb(t_stacks *s);
void	inst_rr(t_stacks *s);
void	inst_rra(t_stacks *s);
void	inst_rrb(t_stacks *s);
void	inst_rrr(t_stacks *s);
void	sort(t_stacks *s);

#endif
