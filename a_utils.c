/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_utils.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:46:50 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	getindexa(t_stacks *s, int n)
{
	int		i;
	int		i2;
	long	prec;

	i = 0;
	i2 = 0;
	prec = LONG_MIN;
	while (i < s->sizea && s->a[i] != n)
	{
		if (s->a[i] < n && s->a[i] > prec)
		{
			i2 = i;
			prec = s->a[i];
		}
		i++;
	}
	if (i >= s->sizea || s->a[i] != n)
		i = i2;
	return (i);
}

static void	insert_limita(t_stacks *s)
{
	while (s->min != s->a[0])
	{
		if (getindexa(s, s->max) < s->sizea / 2)
			inst_ra(s);
		else
			inst_rra(s);
	}
}

static void	insert_middlea(t_stacks *s)
{
	while (!(s->a[0] > s->b[0] && s->a[s->sizea - 1] < s->b[0]))
	{
		if (getindexa(s, s->b[0]) < s->sizea / 2)
			inst_ra(s);
		else
			inst_rra(s);
	}
}

void	inserta(t_stacks *s)
{
	if (s->b[0] > s->max || s->b[0] < s->min)
		insert_limita(s);
	else
		insert_middlea(s);
	inst_pa(s);
	if (s->a[0] > s->max)
		s->max = s->a[0];
	else if (s->a[0] < s->min)
		s->min = s->a[0];
}
