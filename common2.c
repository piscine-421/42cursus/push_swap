/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:46:56 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	inst_sb(t_stacks *s)
{
	int	swap;

	if (s->sizeb >= 2)
	{
		swap = s->b[0];
		s->b[0] = s->b[1];
		s->b[1] = swap;
	}
	s->instnum++;
	if (s->print)
		ft_printf("sb\n");
}

void	inst_ss(t_stacks *s)
{
	int	swap;

	if (s->sizea >= 2)
	{
		swap = s->a[0];
		s->a[0] = s->a[1];
		s->a[1] = swap;
	}
	if (s->sizeb >= 2)
	{
		swap = s->b[0];
		s->b[0] = s->b[1];
		s->b[1] = swap;
	}
	s->instnum++;
	if (s->print)
		ft_printf("ss\n");
}

void	inst_pa(t_stacks *s)
{
	int	i;

	if (s->sizeb)
	{
		i = s->sizea;
		while (i--)
			s->a[i + 1] = s->a[i];
		s->a[0] = s->b[0];
		s->sizea++;
		i = 0;
		while (s->sizeb > i++)
			s->b[i - 1] = s->b[i];
		s->sizeb--;
	}
	s->instnum++;
	if (s->print)
		ft_printf("pa\n");
}

void	inst_pb(t_stacks *s)
{
	int	i;

	if (s->sizea)
	{
		i = s->sizeb;
		while (i--)
			s->b[i + 1] = s->b[i];
		s->b[0] = s->a[0];
		s->sizeb++;
		i = 0;
		while (s->sizea > i++)
			s->a[i - 1] = s->a[i];
		s->sizea--;
	}
	s->instnum++;
	if (s->print)
		ft_printf("pb\n");
}

void	inst_ra(t_stacks *s)
{
	int	i;
	int	swap;

	i = 0;
	swap = s->a[0];
	while (s->sizea > i++)
		s->a[i - 1] = s->a[i];
	s->a[i - 2] = swap;
	s->instnum++;
	if (s->print)
		ft_printf("ra\n");
}
