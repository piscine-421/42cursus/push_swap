/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   datatypes2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:07 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	prthexl(unsigned long n, int *r)
{
	return (ft_putnbr_base(n, "0123456789abcdef", r));
}

int	prthexu(unsigned long n, int *r)
{
	return (ft_putnbr_base(n, "0123456789ABCDEF", r));
}
