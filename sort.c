/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:47:08 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static size_t	getcost(t_stacks *s, int i)
{
	size_t	cost;
	int		costmod;
	int		n;
	int		racount;
	int		rracount;

	cost = 0;
	n = s->a[i];
	racount = 0;
	rracount = 0;
	if (i < s->sizea / 2)
		racount = i;
	else
		rracount = s->sizea - i;
	if (s->a[i] > s->max || s->a[i] < s->min)
		n = s->max;
	if (getindexb(s, n) <= s->sizeb / 2)
		costmod = getindexb(s, n) - racount;
	else
		costmod = s->sizeb - getindexb(s, n) - rracount;
	if (costmod < 0)
		costmod = 0;
	cost += costmod + rracount + racount + 1;
	return (cost);
}

static void	insert_cheapest(t_stacks *s)
{
	size_t	cost;
	size_t	lowcost;
	int		i;
	int		i2;

	cost = 0;
	while (s->sizea > 3)
	{
		lowcost = SIZE_T_MAX;
		i = 0;
		i2 = 0;
		while (i < s->sizea)
		{
			cost = getcost(s, i);
			if (cost < lowcost)
			{
				lowcost = cost;
				i2 = i;
			}
			i++;
		}
		insertb(s, i2);
	}
}

static void	sort_3(t_stacks *s)
{
	s->min = INT_MAX;
	s->max = INT_MIN;
	if (s->a[0] < s->a[1] && s->a[1] > s->a[2] && s->a[0] < s->a[2])
		inst_rra(s);
	if (s->a[0] > s->a[1] && s->a[1] < s->a[2] && s->a[0] < s->a[2])
		inst_sa(s);
	if (s->a[0] > s->a[1] && s->a[1] > s->a[2] && s->a[0] > s->a[2])
		inst_sa(s);
	if (s->a[0] < s->min)
		s->min = s->a[0];
	if (s->a[1] < s->min)
		s->min = s->a[1];
	if (s->a[2] < s->min)
		s->min = s->a[2];
	if (s->a[0] > s->max)
		s->max = s->a[0];
	if (s->a[1] > s->max)
		s->max = s->a[1];
	if (s->a[2] > s->max)
		s->max = s->a[2];
}

static void	set_limits(t_stacks *s)
{
	if (s->sizeb == 1)
	{
		s->max = s->b[0];
		s->min = s->b[0];
	}
	if (s->sizeb >= 2 && s->b[0] > s->b[1])
	{
		s->max = s->b[0];
		s->min = s->b[1];
	}
	if (s->sizeb >= 2 && s->b[0] < s->b[1])
	{
		s->max = s->b[1];
		s->min = s->b[0];
	}
}

void	sort(t_stacks *s)
{
	if (s->sizea > 3)
		inst_pb(s);
	if (s->sizea > 3)
		inst_pb(s);
	set_limits(s);
	insert_cheapest(s);
	sort_3(s);
	while (s->sizeb)
		inserta(s);
	while (s->min != s->a[0])
	{
		if (getindexa(s, s->min) <= s->sizea / 2)
			inst_ra(s);
		else
			inst_rra(s);
	}
}
