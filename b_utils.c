/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_utils.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/12/18 23:46:52 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	getindexb(t_stacks *s, int n)
{
	int		i;
	int		i2;
	long	prec;

	i = 0;
	i2 = 0;
	prec = LONG_MIN;
	while (i < s->sizeb && s->b[i] != n)
	{
		if (s->b[i] < n && s->b[i] > prec)
		{
			i2 = i;
			prec = s->b[i];
		}
		i++;
	}
	if (i >= s->sizeb || s->b[i] != n)
		i = i2;
	return (i);
}

static void	insert_limitb(t_stacks *s, int *racount, int *rracount)
{
	while (s->max != s->b[0])
	{
		if (getindexb(s, s->max) <= s->sizeb / 2)
		{
			if (*racount)
			{
				inst_rr(s);
				*racount -= 1;
			}
			else
				inst_rb(s);
		}
		else
		{
			if (*rracount)
			{
				inst_rrr(s);
				*rracount -= 1;
			}
			else
				inst_rrb(s);
		}
	}
}

static void	insert_middleb(t_stacks *s, int *racount, int *rracount, int n)
{
	while (!(s->b[0] < n && s->b[s->sizeb - 1] > n))
	{
		if (getindexb(s, n) <= s->sizeb / 2)
		{
			if (*racount)
			{
				inst_rr(s);
				*racount -= 1;
			}
			else
				inst_rb(s);
		}
		else
		{
			if (*rracount)
			{
				inst_rrr(s);
				*rracount -= 1;
			}
			else
				inst_rrb(s);
		}
	}
}

void	insertb(t_stacks *s, const int i)
{
	int	racount;
	int	rracount;

	racount = 0;
	rracount = 0;
	if (i < s->sizea / 2)
		racount = i;
	else
		rracount = s->sizea - i;
	if (s->a[i] > s->max || s->a[i] < s->min)
		insert_limitb(s, &racount, &rracount);
	else
		insert_middleb(s, &racount, &rracount, s->a[i]);
	while (racount && racount--)
		inst_ra(s);
	while (rracount && rracount--)
		inst_rra(s);
	inst_pb(s);
	if (s->b[0] > s->max)
		s->max = s->b[0];
	else if (s->b[0] < s->min)
		s->min = s->b[0];
}
